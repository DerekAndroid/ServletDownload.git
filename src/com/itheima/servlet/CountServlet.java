package com.itheima.servlet;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取ServletContext对象
		ServletContext sc = getServletContext();
		//从ServletContext中获取访问次数
		Integer count=(Integer)sc.getAttribute("count");
		//判断用户是否是第一次访问
		if(count==null){
			count=1;
		}else{
			count++;
		}
		//把得到的count值放入ServletContext中
		sc.setAttribute("count", count);
		
		System.out.println(sc.getAttribute("count"));
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
