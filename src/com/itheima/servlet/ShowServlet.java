package com.itheima.servlet;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//创建ServletContext对象
		ServletContext sc = getServletContext();
		Integer count=(Integer)sc.getAttribute("count");
		//从ServletContext对象中获取访问次数  count
		//三元运算需要用括号包裹起来   因为是一个表达式
		System.out.println("您访问的次数为："+(count==null?0:count));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
